use std::{error, fmt, io};

#[derive(Debug)]
pub enum Error {
    Connection(xcb::ConnError),
    Io(io::Error),
    NoScreensaver,
    ScreensaverFailed(&'static str),
    XcbError(u8),
    Dbus(dbus::Error),
    DbusDisconnected,
    NoSession,
}
pub type Result<T> = std::result::Result<T, Error>;

impl fmt::Display for Error {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        use Error::*;
        match self {
            Connection(e) => e.fmt(fmt),
            Io(e) => e.fmt(fmt),
            NoScreensaver => fmt.write_str("No screensaver extension is available"),
            ScreensaverFailed(msg) => write!(fmt, "Error while registering screensaver: {}", msg),
            // TODO: shouuld we have human-readable messages for this?
            XcbError(error_code) => {
                write!(fmt, "Received error message from X of type {}", error_code)
            }
            Dbus(e) => e.fmt(fmt),
            DbusDisconnected => fmt.write_str("Lost connection to dbus"),
            NoSession => fmt.write_str("No logind session available"),
        }
    }
}
impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        use Error::*;
        match self {
            Connection(e) => Some(e),
            Io(e) => Some(e),
            Dbus(e) => Some(e),
            _ => None,
        }
    }
}

impl From<xcb::ConnError> for Error {
    fn from(other: xcb::ConnError) -> Self {
        Error::Connection(other)
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Error::Io(err)
    }
}

impl From<dbus::Error> for Error {
    fn from(err: dbus::Error) -> Self {
        Error::Dbus(err)
    }
}
