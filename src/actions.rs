use std::env;
use std::ffi::OsString;
use std::process::Command;

use clap::{clap_app, crate_version, ArgMatches};

pub struct Actions {
    shell: OsString,
    idle: Option<OsString>,
    resume: Option<OsString>,
    sleep: Option<OsString>,
    wake: Option<OsString>,
    lock: Option<OsString>,
    unlock: Option<OsString>,
}

macro_rules! action_fn {
    ($name:ident, $has_name:ident) => {
        pub fn $name(&self) {
            self.run(&self.$name);
        }

        pub fn $has_name(&self) -> bool {
            self.$name.is_some()
        }
    }
}

impl Actions {
    pub fn parse() -> Self {
        parse_args()
    }

    pub fn idle_change(&self, is_idle: bool) {
        if is_idle {
            self.run(&self.idle);
        } else {
            self.run(&self.resume);
        }
    }

    action_fn!(sleep, has_sleep);
    action_fn!(wake, has_wake);
    action_fn!(lock, has_lock);
    action_fn!(unlock, has_unlock);

    fn run(&self, command: &Option<OsString>) {
        if let Some(ref cmd) = command {
            let status = Command::new(&self.shell).arg("-c").arg(cmd).status();
            match status {
                Ok(status) if !status.success() => eprintln!(
                    "Non-zero status code while running `{}`: {}",
                    cmd.to_string_lossy(),
                    status.code().unwrap_or(-1)
                ),
                Err(err) => eprintln!(
                    "Error while attempting to spawn process `{}`: {}",
                    cmd.to_string_lossy(),
                    err
                ),
                _ => {}
            }
        }
    }
}

// TODO: support timeouts?
fn parse_args() -> Actions {
    let matches = clap_app!(app =>
        (name: "xidle")
        (version: crate_version!())
        (about: "Run commands to idle events (and others)")
        (@arg IDLE: -i --idle [COMMAND] "Execute command as soon as the x screensaver API activates")
        (@arg RESUME: -r --resume [COMMAND] "Command to run when there is activity after the session has been marke as idle")
        (@arg SLEEP: -s --sleep [COMMAND] "Command to run before the system is suspended (sleep)")
        (@arg WAKE: -a --wake [COMMAND] "Command to run after the system wakes up from suspend (sleep)")
        (@arg LOCK: -l --lock [COMMAND] "Command to run when logind requests the session to lock")
        (@arg UNLOCK: -u --unlock [COMMAND] "Command to run when logind requests the session to unlock")
    ).get_matches();

    Actions {
        shell: env::var_os("SHELL").unwrap_or("/bin/sh".into()),
        idle: get_command(&matches, "IDLE"),
        resume: get_command(&matches, "RESUME"),
        sleep: get_command(&matches, "SLEEP"),
        wake: get_command(&matches, "WAKE"),
        lock: get_command(&matches, "LOCK"),
        unlock: get_command(&matches, "UNLOCK"),
    }
}

fn get_command(matches: &ArgMatches, key: &str) -> Option<OsString> {
    matches.value_of_os(key).map(|cmd| cmd.to_owned())
}
