use mio::{Events, Poll, PollOpt, Ready, Token};
use signal_hook::iterator::Signals;

mod error;
use error::Result;

mod idle;
use idle::*;

mod logind;
use logind::Logind;

mod actions;
use actions::Actions;

const IDLE_TOKEN: Token = Token(0);
const SIGNAL_TOKEN: Token = Token(1);
const SIGNAL_LOGIND: Token = Token(2);

fn main() -> Result<()> {
    let actions = Actions::parse();
    let poll = Poll::new()?;
    let mut watcher = IdleWatcher::register(&poll)?;

    let mut events = Events::with_capacity(256);

    let signals = Signals::new(&[signal_hook::SIGINT, signal_hook::SIGTERM])?;

    let mut logind = Logind::new(&actions)?;

    poll.register(&signals, SIGNAL_TOKEN, Ready::readable(), PollOpt::level())?;
    poll.register(&logind, SIGNAL_LOGIND, Ready::readable(), PollOpt::level())?;

    loop {
        poll.poll(&mut events, None)?;

        for event in events.iter() {
            match event.token() {
                IDLE_TOKEN => {
                    if let Some(idle) = watcher.handle_events()? {
                        logind.set_idle(idle);
                        actions.idle_change(idle);
                    }
                }
                SIGNAL_TOKEN => {
                    // TODO: enter idle state on SIGUSR1?
                    return Ok(());
                }
                SIGNAL_LOGIND => {
                    logind.process_messages()?;
                }
                _ => unreachable!(),
            }
        }
    }
}
