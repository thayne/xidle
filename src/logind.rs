use dbus::arg::OwnedFd;
use dbus::channel::{BusType, Channel};
use dbus::message::{MatchRule, Message};
use dbus::strings::*;
use mio::event::Evented;
use mio::unix::EventedFd;
use mio::{Poll, PollOpt, Ready, Token};

use std::{env, io, time::Duration};

use crate::actions::Actions;
use crate::error::{self, Error, Result};

const LOGIN1_ADDR: &'static str = "org.freedesktop.login1\0";
const MANAGER_PATH: &'static str = "/org/freedesktop/login1\0";
const MANAGER_IFACE: &'static str = "org.freedesktop.login1.Manager\0";
const SESSION_IFACE: &'static str = "org.freedesktop.login1.Session\0";

const TIMEOUT: Duration = Duration::from_millis(10);
const ZERO_SECS: Duration = Duration::from_secs(0);

pub struct Logind<'a> {
    actions: &'a Actions,
    channel: Channel,
    session: Path<'static>,
    inhibit_fd: Option<OwnedFd>,
    inhibit_serial: Option<u32>,
    sleep_match: MatchRule<'static>,
    lock_match: MatchRule<'static>,
    unlock_match: MatchRule<'static>,
}

impl<'a> Logind<'a> {
    pub fn new(actions: &'a Actions) -> Result<Self> {
        let mut channel = Channel::get_private(BusType::System)?;

        let session = get_session(&channel)?;

        channel.set_watch_enabled(true);

        let sleep_match = sleep_match();
        let lock_match = lock_match();
        let unlock_match = unlock_match();
        let mut logind = Self {
            actions,
            channel,
            session,
            inhibit_fd: None,
            inhibit_serial: None,
            sleep_match,
            lock_match,
            unlock_match,
        };

        if actions.has_sleep() || actions.has_wake() {
            add_match(&logind.channel, &logind.sleep_match);
            logind.acquire_inhibitor();
        }

        if actions.has_lock() {
            add_match(&logind.channel, &logind.lock_match);
        }

        if actions.has_unlock() {
            add_match(&logind.channel, &logind.unlock_match);
        }
        logind.channel.flush(); // make sure we send any pending messages.

        Ok(logind)
    }

    pub fn set_idle(&self, idle: bool) {
        let mut msg = Message::call_with_args(
            LOGIN1_ADDR,
            &self.session,
            SESSION_IFACE,
            "SetIdleHint\0",
            (idle,),
        );
        msg.set_no_reply(true);
        let _ = self.channel.send(msg);
        self.channel.flush();
    }

    pub fn process_messages(&mut self) -> Result<()> {
        use dbus::message::MessageType::*;
        self.channel
            .read_write(Some(ZERO_SECS))
            .map_err(|_| error::Error::DbusDisconnected)?;
        while let Some(msg) = self.channel.pop_message() {
            match msg.msg_type() {
                Signal => self.process_signal(&msg),
                MethodReturn => self.process_return(&msg),
                Error => eprintln!(
                    "Received a DBUS error: {}",
                    msg.get1::<&str>().unwrap_or("UNKNOWN")
                ),
                MethodCall => {
                    if let Some(response) = dbus::channel::default_reply(&msg) {
                        let _ = self.channel.send(response);
                    }
                }
            }
        }
        self.channel.flush();
        Ok(())
    }

    fn process_return(&mut self, msg: &Message) {
        if msg.get_reply_serial() == self.inhibit_serial {
            self.inhibit_fd = msg.get1();
            self.inhibit_serial = None;
        }
        // otherwise we don't care (or expect a return message)
    }

    fn process_signal(&mut self, msg: &Message) {
        if self.sleep_match.matches(msg) {
            match msg.get1::<bool>() {
                Some(true) => {
                    self.actions.sleep();
                    self.inhibit_fd.take(); // drop the inhibit lock
                }
                Some(false) => {
                    self.acquire_inhibitor();
                    self.actions.wake();
                }
                None => eprintln!("Unexpected PrepareForSleep message"),
            }
        } else if self.lock_match.matches(msg) {
            self.actions.lock();
        } else if self.unlock_match.matches(msg) {
            self.actions.unlock();
        }
    }

    fn acquire_inhibitor(&mut self) {
        let msg = Message::call_with_args(
            LOGIN1_ADDR,
            MANAGER_PATH,
            MANAGER_IFACE,
            "Inhibit",
            (
                "sleep",
                "xidle",
                "Launch program (such as locker) before sleep",
                "delay",
            ),
        );

        // Just send the message, we'll handle getting the message in the handler
        self.inhibit_serial = self.channel.send(msg).ok();
    }
}

impl<'a> Evented for Logind<'a> {
    fn register(
        &self,
        poll: &Poll,
        token: Token,
        interest: Ready,
        opts: PollOpt,
    ) -> io::Result<()> {
        let fd = self.channel.watch().fd;
        EventedFd(&fd).register(poll, token, interest, opts)
    }

    fn reregister(
        &self,
        poll: &Poll,
        token: Token,
        interest: Ready,
        opts: PollOpt,
    ) -> io::Result<()> {
        let fd = self.channel.watch().fd;
        EventedFd(&fd).reregister(poll, token, interest, opts)
    }

    fn deregister(&self, poll: &Poll) -> io::Result<()> {
        let fd = self.channel.watch().fd;
        EventedFd(&fd).deregister(poll)
    }
}

fn get_session(chan: &Channel) -> Result<Path<'static>> {
    let session_id = match env::var("XDG_SESSION_ID") {
        Ok(sess_id) => sess_id,
        Err(_) => return Ok("/org/freedesktop/login1/auto\0".into()),
    };

    let req = Message::call_with_args(
        LOGIN1_ADDR,
        MANAGER_PATH,
        MANAGER_IFACE,
        "GetSession\0",
        (session_id,),
    );
    let resp = chan.send_with_reply_and_block(req, TIMEOUT)?;
    resp.get1().ok_or(Error::NoSession)
}

fn add_match(chan: &Channel, rule: &MatchRule) {
    let mut msg = Message::call_with_args(
        "org.freedesktop.DBus\0",
        "/org/freedesktop/DBus\0",
        "org.freedesktop.DBus\0",
        "AddMatch\0",
        (rule.match_str(),),
    );
    msg.set_no_reply(true);
    chan.send(msg).unwrap();
}

fn sleep_match() -> MatchRule<'static> {
    MatchRule::new_signal(MANAGER_IFACE, "PrepareForSleep\0")
}

fn lock_match() -> MatchRule<'static> {
    MatchRule::new_signal(SESSION_IFACE, "Lock\0")
}

fn unlock_match() -> MatchRule<'static> {
    MatchRule::new_signal(SESSION_IFACE, "Unlock\0")
}
