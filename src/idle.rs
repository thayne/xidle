use std::os::unix::io::AsRawFd;

use mio::{unix::EventedFd, Poll, PollOpt, Ready};
use xcb::screensaver;

use crate::error::{Error, Result};
use crate::IDLE_TOKEN;

pub struct IdleWatcher {
    conn: xcb::Connection,
    root: xcb::Drawable,
    idle: bool,
    event_id: u8,
    property: xcb::Atom,
}

const SCREEN_SAVER_PROPERTY_NAME: &'static str = "_MIT_SCREEN_SAVER_ID";

impl IdleWatcher {
    pub fn register(poll: &Poll) -> Result<Self> {
        let (conn, screen_number) = xcb::Connection::connect(None)?;

        let xid = conn.generate_id();
        let atom_cookie = xcb::intern_atom(&conn, false, SCREEN_SAVER_PROPERTY_NAME);
        let event_id = conn
            .get_extension_data(screensaver::id())
            .filter(|data| data.present())
            .ok_or(Error::NoScreensaver)
            .map(|data| data.first_event())?;
        let root = conn
            .get_setup()
            .roots()
            .nth(screen_number as usize)
            .map(|s| s.root())
            .unwrap_or(0);
        screensaver::set_attributes(
            &conn,
            root,
            -1,
            -1,
            1,
            1,
            0,
            xcb::COPY_FROM_PARENT as u8,
            xcb::COPY_FROM_PARENT as u8,
            xcb::COPY_FROM_PARENT,
            &[],
        )
        .request_check()
        .map_err(|_| {
            Error::ScreensaverFailed("Unable to set attributes. Is another screensaver running?")
        })?;
        screensaver::select_input(&conn, root, screensaver::EVENT_NOTIFY_MASK);

        let prop_atom = atom_cookie.get_reply().unwrap().atom();
        // TODO: figure out why this doesn't work
        let a = xcb::change_property(
            &conn,
            xcb::PROP_MODE_REPLACE as u8,
            root,
            prop_atom,
            xcb::ATOM_INTEGER,
            32,
            &[xid],
        );
        a.request_check().unwrap();

        poll.register(
            &EventedFd(&conn.as_raw_fd()),
            IDLE_TOKEN,
            Ready::readable(),
            PollOpt::edge(),
        )?;

        Ok(IdleWatcher {
            conn,
            root,
            event_id,
            idle: false,
            property: prop_atom,
        })
    }

    /// Read events from the queue and return if the idle state changed.
    ///
    /// If the state did change then the returne value is Some(idle) where idle
    /// is true if the session is now idle.
    ///
    /// None is returned if the state hasn't changed.
    pub fn handle_events(&mut self) -> Result<Option<bool>> {
        let mut idle = self.idle;
        while let Some(event) = self.conn.poll_for_event() {
            let tp = event.response_type();
            if tp == self.event_id {
                let event: &screensaver::NotifyEvent = unsafe { xcb::cast_event(&event) };
                match event.state() as u32 {
                    screensaver::STATE_ON => {
                        idle = true;
                    }
                    screensaver::STATE_OFF => {
                        idle = false;
                    }
                    _ => unreachable!(),
                }
            } else if tp == 0 {
                // This is safe because 0 is the response type for an error event
                let error: &xcb::GenericError = unsafe { xcb::cast_event(&event) };
                return Err(Error::XcbError(error.error_code()));
            }
        }
        let event = if idle != self.idle {
            self.idle = idle;
            Some(idle)
        } else {
            None
        };

        Ok(event)
    }
}

impl Drop for IdleWatcher {
    fn drop(&mut self) {
        screensaver::unset_attributes(&self.conn, self.root);
        xcb::delete_property_checked(&self.conn, self.root, self.property)
            .request_check()
            .unwrap();
    }
}
